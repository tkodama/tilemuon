解析の手順

・tilemuonというフォルダをダウンロード&解凍  

<br/>
・lxplusにログイン

>$ ssh username@lxplus.cern.ch
>
>$ setup ATLAS
>
>$ lsetup root  

<br/>
・decodeするためのコードをチェックアウト(初回のみ)

>$ svn co
>
>$ svn+ssh://svn.cern.ch/reps/atlas-okumura/okumura/tileMuonDecoder/trunk tileMuonDecoder
>
>$ cd tileMuonDecoder
>
>$ make  

<br/>
・データのコピー&解凍

>$ source /afs/cern.ch/atlas/project/tdaq/cmake/cmake_tdaq/bin/cm_setup.sh tdaq-07-01-00 x86_64-slc6-gcc62-opt 

↑初回のみ


>$ eos cp < .dataのfilepath >
>
>$ eformat-decompress.py < filename > < decompress後のfilename >  


<br/>
・デコード

>$ ./decoder -I < decompress後のfilename >


->output.root というファイルが出来る


>$ exit  

<br/>
・lxplusから自分のパソコンにデータをコピー

>$ scp username@lxplus.cern.ch:/< output.rootのfilepath > <fileを持ってきたい場所>  


<br/>
・解析コードの実行

>$ cd < tilemuonのpath >
>
>$ ./RunAnalysis -I output.root  

作成されるファイル

・output_EventCount.pdf:

各セクター毎のイベント数を表した１次元ヒスト

<br/>
・GER_Aside.pdf:

Asideの各セクターのGoodEventRateを表した１次元ヒスト

<br/>
・GER_total_Aside.pdf:

A-sideの各セクターのprev,curr,nextのGoodEventRateの合計を表した１次元ヒスト

<br/>
・C-sideについても同様



